# lambda_language

An interpreter for a functional language inspired by lambda calculus.
Read this article http://lisperator.net/pltut/ for more information about lambda language and interpreter code.
All credits to Mihai Bazon.

# Setup project

### Install dependencies
```
npm install
```

### Compile
```
npm run compile
```


# Execute interpreter
```
npm run start -- "<code>"
```

or

```
echo "<code>" | npm run start --
```

## Usage example
```
echo "fib = lambda (n) if n < 2 then n else fib(n - 1) + fib(n - 2);
      println(fib(15));" | npm run start --
```

```
echo 'print-range = lambda(a, b)
                    if a <= b then {  # `then` here is optional as you can see below
                      print(a);
                      if a + 1 <= b {
                        print(", ");
                        print-range(a + 1, b);
                      } else println("");  # newline
                    };
    print-range(1, 5);' | npm run start --
```

test "let" keyword
```
echo 'print((λ loop (n) if n > 0 then n + loop(n - 1)
                           else 0)
      (10));' | npm run start --
```

```
echo 'let (x = 10) {
  let (x = x * 2, y = x * x) {
    println(x);  ## 20
    println(y);  ## 400
  };
  println(x);  ## 10
};' | npm run start --
```


```
echo 'fib = λ(n) if n < 2 then n else fib(n - 1) + fib(n - 2);
time( λ() println(fib(10)) );' | npm run start --
```

```
echo 'fib = λ(n) if n < 2 then n else fib(n - 1) + fib(n - 2);
time( λ() println(fib(20)) );' | npm run start --
```

```
echo 'sum = λ(n, ret)
        if n == 0 then ret
                  else sum(n - 1, ret + n);

time( λ() println(sum(50000, 0)) );' | npm run start --
```


Code for compilation tests.
```
echo '"sum = lambda(x, y) x + y; print(sum(2, 3));"' | npm run start --
```

```
echo 'fib = λ(n) if n < 2 then n else fib(n - 1) + fib(n - 2);
time( λ() println(fib(27)) );' | npm run start --