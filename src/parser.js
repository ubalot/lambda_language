const PRECEDENCE = {
  '=': 1,
  '||': 2,
  '&&': 3,
  '<': 7,
  '>': 7,
  '<=': 7,
  '>=': 7,
  '==': 7,
  '!=': 7,
  '+': 10,
  '-': 10,
  '*': 20,
  '/': 20,
  '%': 20
}

const FALSE = {
  type: 'bool',
  value: false
}

const parse = (tokenStream) => {
  const isPunc = (ch) => {
    const tok = tokenStream.peek()
    return tok && tok.type === 'punc' && (!ch || tok.value === ch) && tok
  }
  const isKw = (kw) => {
    const tok = tokenStream.peek()
    return tok && tok.type === 'kw' && (!kw || tok.value === kw) && tok
  }
  const isOp = (op) => {
    const tok = tokenStream.peek()
    return tok && tok.type === 'op' && (!op || tok.value === op) && tok
  }
  const skipPunc = (ch) => {
    if (isPunc(ch)) tokenStream.next()
    else tokenStream.croak(`Expecting punctuation: "${ch}"`)
  }
  const skipKw = (kw) => {
    if (isKw(kw)) tokenStream.next()
    else tokenStream.croak(`Expecting keyword: "${kw}"`)
  }
  // const skipOp = op => {
  //   if (isOp(op)) tokenStream.next()
  //   else tokenStream.croak('Expecting operator: "' + op + '"')
  // }
  const unexpected = () => {
    tokenStream.croak(`Unexpected token: ${JSON.stringify(tokenStream.peek())}`)
  }

  const delimited = (start, stop, separator, parser) => {
    const a = []
    let first = true
    skipPunc(start)
    while (!tokenStream.eof()) {
      if (isPunc(stop)) break

      if (first) first = false
      else skipPunc(separator)

      if (isPunc(stop)) break
      a.push(parser())
    }
    skipPunc(stop)
    return a
  }

  const parseVarname = () => {
    const name = tokenStream.next()
    if (name.type !== 'var') tokenStream.croak('Expecting variable name')
    return name.value
  }
  const parseIf = () => {
    skipKw('if')
    const cond = parseExpression()
    if (!isPunc('{')) skipKw('then')
    const then = parseExpression()
    const ret = {
      type: 'if',
      cond: cond,
      then: then
    }
    if (isKw('else')) {
      tokenStream.next()
      ret.else = parseExpression()
    }
    return ret
  }
  const parseLambda = () => ({
    type: 'lambda',
    name: tokenStream.peek().type === 'var' ? tokenStream.next().value : null,
    vars: delimited('(', ')', ',', parseVarname),
    body: parseExpression()
  })

  const parseVardef = () => {
    const name = parseVarname()
    let def
    if (isOp('=')) {
      tokenStream.next()
      def = parseExpression()
    }
    return {
      name: name,
      def: def
    }
  }

  const parseLet = () => {
    skipKw('let')
    if (tokenStream.peek().type === 'var') {
      const name = tokenStream.next().value
      const defs = delimited('(', ')', ',', parseVardef)
      return {
        type: 'call',
        func: {
          type: 'lambda',
          name: name,
          vars: defs.map((def) => def.name),
          body: parseExpression()
        },
        args: defs.map((def) => def.def || FALSE)
      }
    }
    return {
      type: 'let',
      vars: delimited('(', ')', ',', parseVardef),
      body: parseExpression()
    }
  }

  const parseBool = () => ({
    type: 'bool',
    value: tokenStream.next().value === 'true'
  })

  const parseCall = func => ({
    type: 'call',
    func: func,
    args: delimited('(', ')', ',', parseExpression)
  })

  const parseProg = () => {
    const prog = delimited('{', '}', ';', parseExpression)
    if (prog.length === 0) return FALSE
    if (prog.length === 1) return prog[0]
    return {
      type: 'prog',
      prog: prog
    }
  }

  const parseAtom = () => maybeCall(() => {
    if (isPunc('(')) {
      tokenStream.next()
      const exp = parseExpression()
      skipPunc(')')
      return exp
    }
    if (isPunc('{')) {
      return parseProg()
    }
    if (isKw('let')) {
      return parseLet()
    }
    if (isKw('if')) {
      return parseIf()
    }
    if (isKw('true') || isKw('false')) {
      return parseBool()
    }
    if (isKw('lambda') || isKw('Î»')) {
      tokenStream.next()
      return parseLambda()
    }
    const tok = tokenStream.next()
    if (tok.type === 'var' || tok.type === 'num' || tok.type === 'str') {
      return tok
    }
    unexpected()
  })

  const maybeBinary = (left, myPrec) => {
    const tok = isOp()
    if (tok) {
      const hisPrec = PRECEDENCE[tok.value]
      if (hisPrec > myPrec) {
        tokenStream.next()
        return maybeBinary({
          type: tok.value === '=' ? 'assign' : 'binary',
          operator: tok.value,
          left: left,
          right: maybeBinary(parseAtom(), hisPrec)
        }, myPrec)
      }
    }
    return left
  }

  const maybeCall = expr => {
    expr = expr()
    return isPunc('(') ? parseCall(expr) : expr
  }

  const parseExpression = () => maybeCall(() => maybeBinary(parseAtom(), 0))

  const parseToplevel = () => {
    const prog = []
    while (!tokenStream.eof()) {
      prog.push(parseExpression())
      if (!tokenStream.eof()) skipPunc(';')
    }
    return {
      type: 'prog',
      prog: prog
    }
  }

  return parseToplevel()
}

export default parse
export { FALSE }
