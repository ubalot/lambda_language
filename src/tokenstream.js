const KEYWORDS = [
  'let', 'if', 'then', 'else', 'lambda', 'Î»', 'true', 'false'
]

const isKeyword = (x) => KEYWORDS.indexOf(x) >= 0

const isDigit = (ch) => /[0-9]/i.test(ch)

const isIdStart = (ch) => /[a-zÎ»_]/i.test(ch)

const isId = (ch) => isIdStart(ch) || '?!-<>=0123456789'.indexOf(ch) >= 0

const isOpChar = (ch) => '+-*/%=&|<>!'.indexOf(ch) >= 0

const isPunc = (ch) => ',;(){}[]'.indexOf(ch) >= 0

const isWhitespace = (ch) => ' \t\n'.indexOf(ch) >= 0

class TokenStream {
  constructor (inputStream) {
    this.input = inputStream
    this.current = null
  }

  readWhile (predicate) {
    let str = ''
    while (!this.input.eof() && predicate(this.input.peek())) {
      str += this.input.next()
    }
    return str
  }
  readNumber () {
    let hasDot = false
    const number = this.readWhile(ch => {
      if (ch === '.') {
        if (hasDot) return false
        hasDot = true
        return true
      }
      return isDigit(ch)
    })
    return {
      type: 'num',
      value: parseFloat(number)
    }
  }
  readIdent () {
    const id = this.readWhile(isId)
    return {
      type: isKeyword(id) ? 'kw' : 'var',
      value: id
    }
  }
  readEscaped (end) {
    let escaped = false
    let str = ''
    this.input.next()
    while (!this.input.eof()) {
      const ch = this.input.next()
      if (escaped) {
        str += ch
        escaped = false
      } else if (ch === '\\') {
        escaped = true
      } else if (ch === end) {
        break
      } else {
        str += ch
      }
    }
    return str
  }
  readString () {
    return {
      type: 'str',
      value: this.readEscaped('"')
    }
  }
  skipComment () {
    this.readWhile(ch => ch !== '\n')
    this.input.next()
  }
  readNext () {
    this.readWhile(isWhitespace)
    if (this.input.eof()) {
      return null
    }
    const ch = this.input.peek()
    if (ch === '#') {
      this.skipComment()
      return this.readNext()
    }
    if (ch === '"') {
      return this.readString()
    }
    if (isDigit(ch)) {
      return this.readNumber()
    }
    if (isIdStart(ch)) {
      return this.readIdent()
    }
    if (isPunc(ch)) {
      return {
        type: 'punc',
        value: this.input.next()
      }
    }
    if (isOpChar(ch)) {
      return {
        type: 'op',
        value: this.readWhile(isOpChar)
      }
    }
    this.input.croak(`Can't handle character: ${ch}`)
  }
  peek () {
    return this.current || (this.current = this.readNext())
  }
  next () {
    const tok = this.current
    this.current = null
    return tok || this.readNext()
  }
  eof () {
    return this.peek() == null
  }
}

export default TokenStream
