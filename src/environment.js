import { GUARD } from './stackGuard'

class Environment {
  constructor (parent) {
    this.vars = Object.create(parent ? parent.vars : null)
    this.parent = parent
  }

  extend () {
    return new Environment(this)
  }

  lookup (name) {
    let scope = this
    while (scope) {
      if (Object.prototype.hasOwnProperty.call(scope.vars, name)) {
        return scope
      }
      scope = scope.parent
    }
  }

  get (name) {
    if (name in this.vars) {
      return this.vars[name]
    }
    throw new Error('Undefined variable ' + name)
  }

  set (name, value) {
    const scope = this.lookup(name)
    if (!scope && this.parent) {
      throw new Error(`Undefined variable ${name}`)
    }
    (scope || this).vars[name] = value
    return value
  }

  def (name, value) {
    this.vars[name] = value
    return value
  }
}

// CPS evaluator
const evaluate = (exp, env, callback) => {
  GUARD(evaluate, arguments)
  switch (exp.type) {
    case 'num':
    case 'str':
    case 'bool':
      callback(exp.value)
      return

    case 'var':
      callback(env.get(exp.value))
      return

    case 'assign':
      if (exp.left.type !== 'var') {
        throw new Error(`Cannot assign to ${JSON.stringify(exp.left)}`)
      }
      // Evaluate right expression first.
      evaluate(exp.right, env, function CC (right) {
        GUARD(CC, arguments)
        callback(env.set(exp.left.value, right))
      })
      return

    case 'binary':
      evaluate(exp.left, env, function CC (left) {
        GUARD(CC, arguments)
        evaluate(exp.right, env, function CC (right) {
          GUARD(CC, arguments)
          callback(applyOp(exp.operator, left, right))
        })
      })
      return

    case 'let':
      (function loop (env, i) {
        GUARD(loop, arguments)
        if (i < exp.vars.length) {
          const v = exp.vars[i]
          if (v.def) {
            evaluate(v.def, env, function CC (value) {
              GUARD(CC, arguments)
              const scope = env.extend()
              scope.def(v.name, value)
              loop(scope, i + 1)
            })
          } else {
            const scope = env.extend()
            scope.def(v.name, false) // initialize to false, it will be evaluated later.
            loop(scope, i + 1)
          }
        } else {
          evaluate(exp.body, env, callback)
        }
      })(env, 0)
      return

    case 'lambda':
      callback(makeLambda(env, exp))
      return

    case 'if':
      evaluate(exp.cond, env, (cond) => {
        if (cond !== false) {
          evaluate(exp.then, env, callback)
        } else if (exp.else) {
          evaluate(exp.else, env, callback)
        } else {
          callback(false)
        }
      })
      return

    case 'prog':
      (function loop (last, i) {
        GUARD(loop, arguments)
        if (i < exp.prog.length) {
          evaluate(exp.prog[i], env, function CC (val) {
            GUARD(CC, arguments)
            loop(val, i + 1)
          })
        } else {
          callback(last)
        }
      })(false, 0)
      return

    case 'call':
      evaluate(exp.func, env, function CC (func) {
        GUARD(CC, arguments)
        const loop = (args, i) => {
          if (i < exp.args.length) {
            evaluate(exp.args[i], env, function CC (arg) {
              GUARD(CC, arguments)
              args[i + 1] = arg
              loop(args, i + 1)
            })
          } else {
            func.apply(null, args)
          }
        }

        loop([ callback ], 0)
      })
      return

    default:
      throw new Error(`I don't know how to evaluate ${exp.type}`)
  }
}

function applyOp (op, a, b) {
  const num = x => {
    if (typeof x !== 'number') {
      throw new Error(`Expected number but got ${x}`)
    }
    return x
  }

  const div = x => {
    if (num(x) === 0) {
      throw new Error('Divide by zero')
    }
    return x
  }

  switch (op) {
    case '+': return num(a) + num(b)
    case '-': return num(a) - num(b)
    case '*': return num(a) * num(b)
    case '/': return num(a) / div(b)
    case '%': return num(a) % div(b)
    case '&&': return a !== false && b
    case '||': return a !== false ? a : b
    case '<': return num(a) < num(b)
    case '>': return num(a) > num(b)
    case '<=': return num(a) <= num(b)
    case '>=': return num(a) >= num(b)
    case '==': return a === b
    case '!=': return a !== b
  }
  throw new Error(`Can't apply operator ${op}`)
}

function makeLambda (env, exp) {
  if (exp.name) {
    env = env.extend()
    env.def(exp.name, lambda)
  }

  function lambda (callback) {
    GUARD(lambda, arguments)
    const names = exp.vars
    const scope = env.extend()
    for (let i = 0; i < names.length; ++i) {
      const value = i + 1 < arguments.length ? arguments[i + 1] : false
      scope.def(names[i], value)
    }
    evaluate(exp.body, scope, callback)
  }

  return lambda
}

export { Environment, evaluate }
