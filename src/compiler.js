import { FALSE } from './parser'

const js = (exp) => {
  switch (exp.type) {
    case 'num' :
    case 'str' :
    case 'bool' :
      return jsAtom(exp)
    case 'var' :
      return jsVar(exp)
    case 'binary' :
      return jsBinary(exp)
    case 'assign' :
      return jsAssign(exp)
    case 'let' :
      return jsLet(exp)
    case 'lambda' :
      return jsLambda(exp)
    case 'if' :
      return jsIf(exp)
    case 'prog' :
      return jsProg(exp)
    case 'call' :
      return jsCall(exp)
    default:
      throw new Error('Dunno how to make_js for ' + JSON.stringify(exp))
  }
}

const jsAtom = (exp) => JSON.stringify(exp.value) // cheating ;-)

const makeVar = (name) => name

const jsVar = (exp) => makeVar(exp.value)

const jsBinary = (exp) => `(${js(exp.left)}${exp.operator}${js(exp.right)})`

// assign nodes are compiled the same as binary
const jsAssign = (exp) => jsBinary(exp)

const jsLambda = (exp) => {
  let code = '(function '
  const CC = exp.name || 'β_CC'
  code += makeVar(CC)
  code += `(${exp.vars.map(makeVar).join(', ')})`
  code += `{ GUARD(arguments, ${CC}); ${js(exp.body)} })`
  return code
}

const jsLet = (exp) => {
  if (exp.vars.length === 0) {
    return js(exp.body)
  }
  const iife = {
    type: 'call',
    func: {
      type: 'lambda',
      vars: [ exp.vars[0].name ],
      body: {
        type: 'let',
        vars: exp.vars.slice(1),
        body: exp.body
      }
    },
    args: [ exp.vars[0].def || FALSE ]
  }
  return `(${js(iife)})`
}

const jsIf = (exp) => '(' +
      js(exp.cond) + ' !== false' +
      ' ? ' + js(exp.then) +
      ' : ' + js(exp.else || FALSE) +
      ')'

const jsProg = (exp) => `(${exp.prog.map(js).join(', ')})`

const jsCall = (exp) => `${js(exp.func)}(${exp.args.map(js).join(', ')})`

const makeJs = (exp) => js(exp)

export default makeJs
