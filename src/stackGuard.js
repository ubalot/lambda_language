// Global variable that count nested function stacks.
let STACKLEN
let IN_EXECUTE = false

// if function stacks are too high an error is raised,
// it will clean the stack.
const GUARD = (f, args) => {
  if (--STACKLEN < 0) {
    throw new Continuation(f, args)
  }
}

class Continuation {
  constructor (f, args) {
    this.f = f
    this.args = args
  }
}

// Wrapper for function call, if there are too much function stacks
// they are clea
const Execute = (f, args) => {
  if (IN_EXECUTE) {
    return f.apply(null, args)
  }
  IN_EXECUTE = true
  while (true) {
    try {
      STACKLEN = 200
      f.apply(null, args)
      break
    } catch (ex) {
      if (ex instanceof Continuation) {
        f = ex.f
        args = ex.args
      } else {
        IN_EXECUTE = false
        throw ex
      }
    }
  }
  IN_EXECUTE = false
}

export { GUARD, Continuation, Execute }
