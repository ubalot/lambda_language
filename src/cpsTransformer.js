import { FALSE } from './parser'

// these are global
let GENSYM = 0

// generate symbols
const gensym = (name) => {
  if (!name) {
    name = ''
  }
  name = 'β_' + name
  return name + (++GENSYM)
}

const hasSideEffects = (exp) => {
  switch (exp.type) {
    case 'call':
    case 'assign':
      return true

    case 'num':
    case 'str':
    case 'bool':
    case 'var':
    case 'lambda':
      return false

    case 'binary':
      return hasSideEffects(exp.left) ||
             hasSideEffects(exp.right)

    case 'if':
      return hasSideEffects(exp.cond) ||
             hasSideEffects(exp.then) ||
             (exp.else && hasSideEffects(exp.else))

    case 'let':
      for (let i = 0; i < exp.vars.length; ++i) {
        let v = exp.vars[i]
        if (v.def && hasSideEffects(v.def)) {
          return true
        }
      }
      return hasSideEffects(exp.body)

    case 'prog':
      for (let i = 0; i < exp.prog.length; ++i) {
        if (hasSideEffects(exp.prog[i])) {
          return true
        }
      }
      return false
  }
  return true
}

const cps = (exp, k) => {
  switch (exp.type) {
    case 'num':
    case 'str':
    case 'bool':
    case 'var':
      return cpsAtom(exp, k)

    case 'assign':
    case 'binary':
      return cpsBinary(exp, k)

    case 'let':
      return cpsLet(exp, k)

    case 'lambda':
      return cpsLambda(exp, k)

    case 'if':
      return cpsIf(exp, k)

    case 'prog':
      return cpsProg(exp, k)

    case 'call':
      return cpsCall(exp, k)

    default:
      throw new Error('Dunno how to CPS ' + JSON.stringify(exp))
  }
}

const cpsAtom = (exp, k) => k(exp)

const cpsBinary = (exp, k) =>
  cps(exp.left, (left) =>
    cps(exp.right, (right) =>
      k({
        type: exp.type,
        operator: exp.operator,
        left: left,
        right: right
      })
    )
  )

const cpsLet = (exp, k) =>
  exp.vars.length === 0
    ? cps(exp.body, k)
    : cps({
      type: 'call',
      args: [ exp.vars[0].def || FALSE ],
      func: {
        type: 'lambda',
        vars: [ exp.vars[0].name ],
        body: {
          type: 'let',
          vars: exp.vars.slice(1),
          body: exp.body
        }
      }
    }, k)

const cpsLambda = (exp, k) => {
  const cont = gensym('K')
  const body = cps(exp.body, (body) => ({
    type: 'call',
    func: {
      type: 'var',
      value: cont
    },
    args: [ body ]
  }))
  return k({
    type: 'lambda',
    name: exp.name,
    vars: [ cont ].concat(exp.vars),
    body: body
  })
}

const cpsIf = (exp, k) =>
  cps(exp.cond, (cond) => {
    const cvar = gensym('I')
    const cast = makeContinuation(k)
    k = (ifresult) => ({
      type: 'call',
      func: {
        type: 'var',
        value: cvar },
      args: [ ifresult ]
    })

    return {
      type: 'call',
      func: {
        type: 'lambda',
        vars: [ cvar ],
        body: {
          type: 'if',
          cond: cond,
          then: cps(exp.then, k),
          else: cps(exp.else || FALSE, k)
        }
      },
      args: [ cast ]
    }
  })

const cpsCall = (exp, k) =>
  cps(exp.func, (func) =>
    (function loop (args, i) {
      if (i === exp.args.length) {
        return {
          type: 'call',
          func: func,
          args: args
        }
      }
      return cps(exp.args[i], (value) => {
        args[i + 1] = value
        return loop(args, i + 1)
      })
    })([ makeContinuation(k) ], 0)
  )

const makeContinuation = (k) => {
  const cont = gensym('R')
  return {
    type: 'lambda',
    vars: [ cont ],
    body: k({
      type: 'var',
      value: cont
    })
  }
}

const cpsProg = (exp, k) =>
  (function loop (body) {
    if (body.length === 0) {
      return k(FALSE)
    }
    if (body.length === 1) {
      return cps(body[0], k)
    }
    if (!hasSideEffects(body[0])) {
      return loop(body.slice(1))
    }
    return cps(body[0], (first) => {
      if (hasSideEffects(first)) {
        return {
          type: 'prog',
          prog: [ first, loop(body.slice(1)) ]
        }
      }
      return loop(body.slice(1))
    })
  })(exp.prog)

const toCps = (exp, k) => cps(exp, k)

export default toCps
export { gensym, hasSideEffects }
