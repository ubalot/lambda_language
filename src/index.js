import InputStream from './inputstream'
import TokenStream from './tokenstream'
import parse from './parser'
import { Environment, evaluate } from './environment'
import { Execute, GUARD } from './stackGuard'
import makeJs from './compiler'
import toCps from './cpsTransformer'
import optimize from './optimizer'

/* -----[ entry point for NodeJS ]----- */

const UglifyJS = require('uglify-js')

// beta char
const beta = String.fromCharCode(parseInt('feff03b2', 16))

const globalEnv = new Environment()

// call-with-current-continuation function
globalEnv.def('CallCC', function CallCC (callback, func) {
  GUARD(CallCC, arguments)
  func(callback, function CC (discarded, ret) {
    GUARD(CC, arguments)
    callback(ret)
  })
})

// Return current time when invoked.
globalEnv.def('time', (callback, func) => {
  console.time('time')
  func((ret) => {
    console.timeEnd('time')
    callback(ret)
  })
})

// Do nothing function, it stops program execution.
globalEnv.def('halt', (callback) => {})

// Delay program execution.
// Note: program execution is not frozen.
globalEnv.def('sleep', (callback, milliseconds) => {
  // We have to use Execute, since setTimeout will abandon the current stack
  // frame.
  setTimeout(() => {
    Execute(callback, [ false ]) // continuations expect a value, pass false
  }, milliseconds)
})

// Primitivies for writing and reading files.
globalEnv.def("readFile", (callback, filename) => {
  const fs = require('fs')
  fs.readFile(filename, (err, data) => {
    // error handling is a bit more complex, ignoring for now
    Execute(callback, [ data ]) // hope it's clear why we need the Execute
  })
})
globalEnv.def('writeFile', (callback, filename, data) => {
  const fs = require('fs')
  fs.writeFile(filename, data, (err) => {
    Execute(callback, [ false ])
  })
})

// if (typeof process !== 'undefined') {
//   globalEnv.def('println', (callback, val) => {
//     // print 'val' to stdout with trailing newline
//     console.log(val)
//     callback(false) // call the continuation with some return value
//                     // if we don't call it, the program would stop
//                     // abruptly after a print!
//   })
//   globalEnv.def('print', (callback, val) => {
//     // print 'val' to stdout without trailing newline
//     process.stdout.write(String(val))
//     callback(false)
//   })

//   let code = ''

//   // Read code from user input.
//   process.stdin.setEncoding('utf8')
//   process.stdin.on('readable', () => {
//     const chunk = process.stdin.read()
//     if (chunk) code += chunk
//   })

//   // Parse and evaluate user input code
//   process.stdin.on('end', () => {
//     // var code = "sum = lambda(x, y) x + y; print(sum(2, 3));";
//     const inputStream = new InputStream(code)
//     const tokenStream = new TokenStream(inputStream)
//     const ast = parse(tokenStream)
//     // Execute lambda language.
//     Execute(evaluate, [ ast, globalEnv, (result) => {
//       console.log(`*** Result: ${result}`)
//     }])

//     // // Compile lambda to javascript and execute it.
//     // const jsCode = make_js(ast)
//     // console.log( UglifyJS.parse(jsCode).print_to_string({ beautify: true }) )
//     // eval(jsCode)

//     // const cps = to_cps(ast, (x) => x)
//     // let out = make_js(cps)
//     // out = UglifyJS.parse(out).print_to_string({ beautify: true })
//     // console.log('output: ' + out)
//     // // eval(out)
//   })
// }

if (typeof process !== 'undefined') {
  const print = (k) => {
    console.log([].slice.call(arguments, 1).join(' '))
    k(false)
  }

  const readStdin = (callback) => {
    let text = ''
    process.stdin.setEncoding('utf8')
    process.stdin.on('readable', () => {
      const chunk = process.stdin.read()
      if (chunk) text += chunk
    })
    process.stdin.on('end', () => {
      callback(text)
    })
  }

  readStdin((code) => {
    const inputStream = new InputStream(code)
    const tokenStream = new TokenStream(inputStream)
    const ast = parse(tokenStream)
    const cps = toCps(ast, (x) => ({
      type: 'call',
      func: { type: 'var', value: `${beta}_TOPLEVEL` }, // "Î²_TOPLEVEL"
      args: [ x ]
    }))

    const opt = optimize(cps)

    let jsc = makeJs(opt)

    jsc = `var ${beta}_TMP;\n\n${jsc}`

    if (opt.env) {
      const vars = Object.keys(opt.env.vars)
      if (vars.length > 0) {
        jsc = `var ${vars.map((name) => makeJs({
          type: 'var',
          value: name
        })).join(', ')};\n\n${jsc}`
      }
    }

    jsc = `"use strict";\n\n${jsc}`

    try {
      console.error(UglifyJS.parse(jsc).print_to_string({
        beautify: true,
        indent_level: 2
      }))
    } catch (ex) {
      console.log(ex)
      throw ex
    }

    console.error('\n\n/*')
    const func = new Function(`${beta}_TOPLEVEL, GUARD, print, require, Execute`, jsc)
    console.time('Runtime')
    Execute(func, [
      (result) => {
        console.timeEnd('Runtime')
        console.error(`***Result: ${result}`)
        console.error('*/')
      },
      GUARD,
      print,
      require,
      Execute
    ])
  })
}
